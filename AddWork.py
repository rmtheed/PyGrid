#!/usr/bin/python

import pygrid
from optparse import OptionParser
import socket

if __name__ == '__main__':
    usage = "useage: %prog [options]"
    parser = OptionParser(usage)
    parser.add_option("-q", "--Query", type="string", dest="QueryID", help="Secify a group ID to Query")
    parser.add_option("-i", "--inputfile", type="string", dest="inputfile", help="Specify Input file")
    parser.add_option("-s", "--server", type="string", dest="serverName", help="HOST namr of machine where server is running")
    (options, args) = parser.parse_args()

    if options.serverName == None:
        HOST = socket.gethostname()
    else:
        HOST = options.serverName

    pygrid.AddWork(HOST, options.inputfile)