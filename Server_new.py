#!/usr/bin/python
import pygrid
import time
import os


def ClearScreen():
	if os.name == 'nt':
		os.system('cls') #on windows
	else:
		os.system('clear') 

if __name__ == '__main__':
	s = pygrid.Server()
	while True:
		time.sleep(1)
		ClearScreen()

		tlist  = s.GetTaskList()
		for task in tlist:
			print str(task.ID) + ' ' + str(task.SENT_BY) + ' ' +  str(task.RUNNING_ON) + ' ' +   str(''.join(str(task.INFO))) + ' ' + str(task.STATE)
	pass

	s.shutdown()
