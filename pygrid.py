#!/usr/bin/python

# PyGrid Module
# include types and classes used in Py Grid

from multiprocessing.managers import SyncManager
import os
import time
import socket
import multiprocessing
from multiprocessing import Process
import Queue
import subprocess
import sys
from sys import stdout
from time import sleep
import shutil
from time import gmtime, strftime
import datetime

def replaceInList(listin, string, new_string):
    copy = []
    for item in listin:
        if type(item) is list:
            copy.append(replaceInList(item, string, new_string))
        else:
            item = item.replace(string, new_string)
            copy.append(item)
    return copy



class TaskState:
    PENDING  = 1
    RUNNING  = 2
    FINISHED = 3


# this class defines the  structure of a work task
class Task:
    def __init__(self):
        # input data
        self.WD = []
        self.EXE = []
        self.ARGS = []
        self.DEPS = []
        self.RET = []

        # working data
        self.INFO = []
        self.RUNNING_ON = []
        self.SENT_BY = []
        self.KILL = False
        self.STATE = TaskState.PENDING
        self.ID = -1



class SharedList(list):
    def getAll(self):
        copy = self
        return copy
    def size(self):
        return len(self)

    def popfront(self):
        if(len(self) == 0):
            return []
        else:
            value = self.pop(0)
            return value
    def getFirstPending(self):
        for item in self:
            if item.STATE == TaskState.PENDING:
                item.STATE = TaskState.RUNNING
                return item


    def clearFinished(self):
        copy = []
        for item in self:
            if not (item.STATE == TaskState.FINISHED):
                copy.append(item)
        self = copy
        
    def updateListItem(self,task):
        index = -1
        i = 0
        for item in self:
            if  (item.SENT_BY == task.SENT_BY) and (item.ID == task.ID):
                index = i
                break
            i+=1
        if not (index == -1):
            self[index].INFO = task.INFO
            self[index].RUNNING_ON = task.RUNNING_ON
            self[index].STATE = task.STATE


    def getupdatedListItem(self,task):
        for item in self:
            if  (item.SENT_BY == task.SENT_BY) and (item.ID == task.ID):
                return item
        # not found
        return task



                






taskList = SharedList()
def get_list():
    return taskList

class QueueManager(SyncManager): pass


class Server:
    def __init__(self):
        self.HOST = socket.gethostname()
        QueueManager.register('taskList', get_list)
        self.m = QueueManager(address=(self.HOST, 50000), authkey=b'abracadabra')
        self.m.start()
        self.taskList = self.m.taskList()

    def getNumUnFinishedTasks(self):
        tlist = self.taskList.getAll()
        count = 0
        for task in tlist:
            if not (task.STATE == TaskState.FINISHED):
                count += 1
        print count
        return count

    def clearFinished(self):
        self.taskList.clearFinished()

    def GetTaskList(self):
        return self.taskList.getAll()


    def shutdown(self):
        self.m.shutdown()





class AddWork:
    def __init__(self, serverName, inputfile):

        self.ID = []
        self.inputFile = inputfile
        self.taskList = []
        self.servertaskList = []
        self.HOST = serverName

        TIME = strftime("%Y-%m-%d_%H-%M-%S", gmtime())
    
        PID = os.getpid()
        self.ID = socket.gethostname() + '_' + str(PID) + '_' +  TIME


        self.GetTasksFromFile()
        self.ConnectToServer()
        # add tasks to server
        for task in self.taskList:
            self.servertaskList.append(task)

    def GetTasksFromFile(self):
        lines = tuple(open(self.inputFile,'r'))

        withinblock = False
        i = 0


        for line in lines:
            line = line.lstrip()
            if line.startswith('//'):   # ignore comments
                continue

    
            # start block
            if line.startswith('{'):
                if(withinblock == True):
                    print 'error parsing input file -- nested work packet detected'
                    sys.exit()
                withinblock = True
                task = Task()
                continue
    
            # finish block
            if line.startswith('}'):
                if(withinblock == False):
                    print 'error parsing input file -- rogue packet end } character'
                    sys.exit()
                withinblock = False

                # check task is valid
                task.ID = i
                #task.STATE = TaskState.PENDING
                task.SENT_BY = self.ID
                i += 1
                self.taskList.append(task)
    
            if line.startswith('WD'):
                if not task.WD == []:
                    print 'Task has multiple WD values'
                    sys.exit()
                task.WD = line.split()[1]
    
            if line.startswith('EXE'):
                if not task.EXE == []:
                    print 'Task has multiple WD values'
                    sys.exit()
                task.EXE = line.split()[1]
    
            if line.startswith('RET'):
                if not task.RET == []:
                    print 'Task has multiple WD values'
                    sys.exit()
                task.RET = line.split()[1]
    
            if line.startswith('DEP'):
                task.DEPS.append(line.split()[1])
            if line.startswith('ARG'):
                task.ARGS.append(line.split()[1:])

    def ConnectToServer(self):

        QueueManager.register('taskList')
        m = QueueManager(address=(self.HOST, 50000), authkey='abracadabra')
        try:
            m.connect()
        except:
            print 'Failed to connect to Server : ' + self.HOST
            sys.exit()
        self.servertaskList  =  m.taskList()
        print 'connected to server started on HOST :' + self.HOST  





class Worker:
    def __init__(self, HOST_LIST):

        self.ID = socket.gethostname() + '_' + str(os.getpid())
        print 'Worker ID : ' + self.ID

        self.HOST_LIST = HOST_LIST
        self.localWD = os.path.abspath(os.getcwd() + '\\WD')
        self.root = os.path.abspath(os.getcwd())
        self.currentHostIndex = 0


        self.task = Task()
        self.taskList = None

        if os.path.exists(self.localWD):
            shutil.rmtree(self.localWD)
        
        os.mkdir(self.localWD)

        self.exitCode = 0

    def resetState(self):
        if not (self.task == None) and not (self.task.ID == -1):
            try:
                self.task.STATE = TaskState.FINISHED
                self.taskList.updateListItem(self.task)
            except:
                pass
        self.task = Task()
        self.exitCode = 0
        os.system('cls')
    def updateInfo(self, string = []):
        if not (self.task.ID == -1):
            if not (string == []):
                self.task.INFO = []
                self.task.INFO.append(string)
            try:
                self.taskList.updateListItem(self.task)
            except:
                pass


    def Connect(self):
        HOST = self.HOST_LIST[self.currentHostIndex]

        self.currentHostIndex += 1
        if self.currentHostIndex >= len(self.HOST_LIST):
            self.currentHostIndex = 0

        class QueueManager(SyncManager): pass
        QueueManager.register('taskList')
        m = QueueManager(address=(HOST, 50000), authkey=b'abracadabra')
        try:
            m.connect()
            self.taskList = m.taskList()
            if(self.taskList.size() == 0):
                return False
            else:
                self.task = self.taskList.getFirstPending()
        except:
            return False

        if self.task == None:
            return False

        self.task.RUNNING_ON = self.ID
        self.updateInfo('SETTING UP')
        print 'connected'
        return True


    def CleanUp_Connect(self):
        print 'failed to connect'
        if not (self.task == None):
            self.updateInfo('FAILED to connect')
        self.resetState()
        time.sleep(1)
        

    def ParseWorkPacket(self):

        # translate localWD into corrent value
        # input data
        self.task.WD = self.task.WD.replace('localWD', self.localWD)
        self.task.EXE = self.task.EXE.replace('localWD', self.localWD)
        self.task.RET = self.task.RET.replace('localWD', self.localWD)
        
        self.task.ARGS  = replaceInList(self.task.ARGS,'localWD',self.localWD)
        self.task.DEPS  = replaceInList(self.task.DEPS,'localWD',self.localWD)

        if self.task.WD == []:
            self.WD = self.localWD

        # check we have all the stuff we need
        if self.task.EXE == [] or self.task.WD == []:
            return False

        # check the DEPs make sense
        for Depend in self.task.DEPS:
            if not os.path.exists(Depend):
                return False

        # print work packet
        print 'Worker ID :'  + str(''.join(str(self.task.ID)))
        print 'EXE       :'  + str(''.join(str(self.task.EXE)))
        print 'DEPS      :'  + str(''.join(str(self.task.DEPS)))
        print 'ARGS      :'  + str(''.join(str(self.task.ARGS)))
        print 'RET       :'  + str(''.join(str(self.task.RET)))
        print 'WD        :'  + str(''.join(str(self.task.WD )))
        print 'SENT_BY   :'  + str(''.join(str(self.task.SENT_BY )))
        print 'RUNNING_ON:'  + str(''.join(str(self.task.RUNNING_ON )))
        print 'ID        :'  + str(''.join(str(self.task.ID )))

        return True


    def CleanUp_ParseWorkPacket(self):
        self.updateInfo("FAILED to parse work task")
        self.resetState()


    def GetDependencies(self):
        for Depend in self.task.DEPS:
            if '\\' in Depend:
                depName = Depend.split('\\')[-1]
                try:
                    shutil.copy2(Depend, self.localWD + '\\' + depName)
                except:
                    return False
            else:
                return False
        return True


    def CleanUp_GetDependencies(self):
        self.updateInfo('FAILED bad dependency')
        self.resetState()



    def RunCommand(self):
        output_file_name = self.localWD + "\log.txt"
        output_file_ptr = open(output_file_name, "w")

        COMMAND = self.task.ARGS
        COMMAND.insert(0,self.task.EXE)

        print 'Running - :'
        self.updateInfo('RUNNING') 
        self.updateInfo()

        #print COMMAND
        #print self.task.WD
        try:
            proc = subprocess.Popen(COMMAND,cwd=self.task.WD, stdout=output_file_ptr,stderr=output_file_ptr)
        except:
            output_file_ptr.flush()
            output_file_ptr.close()
            print 'Failed'
            return False
        
        self.exitCode = proc.poll()

        while self.exitCode == None:

            try:
                self.task = self.taskList.getupdatedListItem(self.task)
            except:
                pass

            if(self.task.KILL == True):
                self.updateInfo('Killing')
                proc.kill()

            # check for info file
            if os.path.exists(self.localWD + '\info.txt'):
                # add to info_output
                file = tuple(open(self.localWD + '\info.txt','r'))
                prev_info = []
                if len(file) > 0:
                    info = file[-1]
                    print info
                    if not (prev_info == []):
                        self.task.INFO.append(prev_info)
                    self.updateInfo(info)
                    prev_info = info
            
            time.sleep(0.5)
            self.exitCode = proc.poll() 

        output_file_ptr.flush()
        output_file_ptr.close()
        return True



    def CleanUp_RunCommand(self):
        self.updateInfo('FAILED - EXE failed to run')
        self.resetState()


    def TidyUp(self):
        # delete deps
        for Depend in self.task.DEPS:
            depName = Depend.split('\\')[-1]
            try:
                os.remove(self.localWD + '\\' + depName)
            except:
                pass


    def CopyDataBack(self):
        # copy everything else back to the RET path  
        if not (self.task.RET == []):    
            self.updateInfo('Copying data to RET')
            st = datetime.datetime.now().strftime("%Y-%m-%d_%H_%M")
            filename = 'copy_log_'+ st + '.txt'
            copylogfile = open(filename, "w")
            try:
                if os.name == 'nt':
                    retval = subprocess.call('robocopy '  + self.localWD + ' ' + self.task.RET + ' /E /MOVE /R:50 /W:30', stdout=copylogfile, stderr=copylogfile,shell=False)
                else:
                    retval = subprocess.call('rsync -r -v '  + self.localWD + ' ' + self.task.RET, stdout=copylogfile, stderr=copylogfile,shell=False)
                copylogfile.close()
                return True
            except:
                copylogfile.close()
                return False
        return True


    def CleanUp_CopyDataBack(self):
        self.updateInfo('FAILED - copy failure')
        self.resetState()


    def CleanUp_Final(self):
        self.updateInfo('FINISHED')
        self.updateInfo()

        if not (self.exitCode  == 0):
            print 'Failed- Run'
        else:
            print 'Finished- Run'

        self.resetState()


    def DoWork(self):
        if not self.Connect():
            self.CleanUp_Connect()
            return
        if not self.ParseWorkPacket():
            self.CleanUp_ParseWorkPacket()
            return 
        if not self.GetDependencies():
            self.CleanUp_GetDependencies()
            return
        if not self.RunCommand():
            self.CleanUp_RunCommand()
            return
        self.TidyUp()
        if not self.CopyDataBack():
            self.CleanUp_CopyDataBack()
            return
        self.CleanUp_Final()

    def MainLoop(self):
        while True:
            if os.path.exists(self.localWD):
                try:
                    shutil.rmtree(self.localWD)
                except:
                    time.sleep(2)
                    continue
        
            os.mkdir(self.localWD)

            time.sleep(1)
            self.DoWork()
