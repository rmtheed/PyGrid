echo off

IF [%1] == [] (
    ECHO enter number of workers needed
    GOTO END 
    )

SET /a i=1

RD  /S /Q c:\workers



set count=%1


if %count% GTR 8 set count=8
if %count% LEQ 0 set count=1



mkdir c:\workers\bin
copy pygrid.py c:\workers\bin\pygrid.py
copy worker.py c:\workers\bin\worker.py
copy startworker.bat c:\workers\bin\startworker.bat

:loop
IF %i% GTR %count% GOTO END

RD c:\workers\worker_%i%
mkdir c:\workers\worker_%i%
START "Worker_%i%" /D"c:\workers\worker_%i%" "c:\workers\bin\startworker.bat" %2 %3



SET /a i=%i%+1
GOTO LOOP

:end


PAUSE

DEL c:\workers\bin\worker.py
RD  /S /Q c:\workers