#!/usr/bin/python

import pygrid
from optparse import OptionParser





if __name__ == '__main__':
    usage = "useage: %prog [options]"
    parser = OptionParser(usage)
    parser.add_option("-s", "--servername", type="string", dest="HOST", help="HOST NAME")
    parser.add_option("-i", "--hostnamefile", type="string", dest="HOST_LISTfile", help="HOST NAME list file input")
    (options, args) = parser.parse_args()

    HOST_LIST = []
    if not(options.HOST_LISTfile is None):
        all_lines = tuple(open(options.HOST_LISTfile,'r'))

        for line in all_lines:
            if (not line.lstrip().startswith("//")) and (not line.isspace()):
                HOST_LIST.append(line)

    if not (options.HOST is None):
        HOST_LIST.append(options.HOST)

    if HOST_LIST == []:
        print "Not Hostnames in list"
        sys.exit()

    mWorker = pygrid.Worker(HOST_LIST)

    mWorker.MainLoop()